
#ifndef MAIN_H
#define MAIN_H

#include <avr/interrupt.h>  
#include <stdio.h>
#include <string.h>
#include <util/delay.h>
#include "onewire.h"
#include "ds18x20.h"

#define sbi(reg,bit) reg |= (1<<bit)
#define cbi(reg,bit) reg &= ~(1<<bit)
#define ibi(reg,bit) reg ^= (1<<bit)
#define CheckBit(reg,bit) (reg&(1<<bit))
#define ARRLEN(x)  (sizeof(x) / sizeof((x)[0]))

#define USART_BAUDRATE_9600 (((F_CPU / (9600 * 16UL))) - 1)

#define BUZZER_PORT PORTB
#define BUZZER_DDR DDRB
#define BUZZER_PIN 1

#define OK_PORT PORTD
#define OK_DDR DDRD
#define OK_PIN 6
#define WARNING_PORT PORTD
#define WARNING_DDR DDRD
#define WARNING_PIN 7
#define ALERT_PORT PORTB
#define ALERT_DDR DDRB
#define ALERT_PIN 0

#define A_DDR DDRB
#define A_PORT PORTB
#define A_PIN 7

#define B_DDR DDRD
#define B_PORT PORTD
#define B_PIN 2
	
#define C_DDR DDRC
#define C_PORT PORTC
#define C_PIN 1

#define D_DDR DDRB
#define D_PORT PORTB
#define D_PIN 5

#define E_DDR DDRB
#define E_PORT PORTB
#define E_PIN 4

#define F_DDR DDRB
#define F_PORT PORTB
#define F_PIN 6

#define G_DDR DDRC
#define G_PORT PORTC
#define G_PIN 2

#define DP_DDR DDRC
#define DP_PORT PORTC
#define DP_PIN 0

#define COM1_DDR DDRD
#define COM1_PORT PORTD
#define COM1_PIN 5

#define COM2_DDR DDRD
#define COM2_PORT PORTD
#define COM2_PIN 4

#define COM3_DDR DDRD
#define COM3_PORT PORTD
#define COM3_PIN 3

#define COM4_DDR DDRC
#define COM4_PORT PORTC
#define COM4_PIN 3

#define ZERO_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);cbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);sbi(G_PORT, G_PIN);
#define ONE_ON() sbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);sbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);sbi(F_PORT, F_PIN);sbi(G_PORT, G_PIN);
#define TWO_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);sbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);cbi(E_PORT, E_PIN);sbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define THREE_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);sbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define FOUR_ON() sbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);sbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define FIVE_ON() cbi(A_PORT, A_PIN);sbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define SIX_ON() cbi(A_PORT, A_PIN);sbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);cbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define SEVEN_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);sbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);sbi(F_PORT, F_PIN);sbi(G_PORT, G_PIN);
#define EIGHT_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);cbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);
#define NINE_ON() cbi(A_PORT, A_PIN);cbi(B_PORT, B_PIN);cbi(C_PORT, C_PIN);cbi(D_PORT, D_PIN);sbi(E_PORT, E_PIN);cbi(F_PORT, F_PIN);cbi(G_PORT, G_PIN);

#define LED_DISPLAY_INIT() A_DDR |= 1<<A_PIN;B_DDR |= 1<<B_PIN;C_DDR |= 1<<C_PIN;D_DDR |= 1<<D_PIN;E_DDR |= 1<<E_PIN;F_DDR |= 1<<F_PIN;G_DDR |= 1<<G_PIN;DP_DDR |= 1<<DP_PIN;COM1_DDR |= 1<<COM1_PIN;COM2_DDR |= 1<<COM2_PIN;COM3_DDR |= 1<<COM3_PIN;COM4_DDR |= 1<<COM4_PIN;

#define LED_DISPLAY_SWITCH_OFF() COM1_PORT &= ~(1<<COM1_PIN);COM2_PORT &= ~(1<<COM2_PIN);COM3_PORT &= ~(1<<COM3_PIN);COM4_PORT &= ~(1<<COM4_PIN);

#define STATE_LED_SWITCH_OFF() cbi(ALERT_PORT, ALERT_PIN);cbi(WARNING_PORT, WARNING_PIN);cbi(OK_PORT, OK_PIN);

#define POWER_SUPPLY_TEMP_WARNING 35
#define POWER_SUPPLY_TEMP_ALERT 40

#define OFFICE_TEMP_WARNING 28
#define OFFICE_TEMP_ALERT 30

#define MEASUREMENT_COUNT 10

#define OFFICE_TEMP_SENSOR_INDEX 7

#define TASCII(numb) (numb+48)

#define true 1
#define false 0

#define vuchar volatile unsigned char 
#define vuint volatile unsigned int

void RunTasks(void);

#endif
