#include "main.h"

static void check_temperature();
static void calculate_average_temperatures();
static void print_devices();
static void show_next_sensor();
static signed char search_next_overheat_sensor(signed char start, signed char end);
unsigned char is_overheat(signed char sensor);
static void print_address(unsigned char* address);
static unsigned char search_ow_devices(void);
static void init_mc35i();
static void mc35i_send_next_msg();
static void mc35i_write_msg_text();
static void mc35i_init_msg_mode();
static void io_init();
static void timer_init();
static void USART_init();
static void USART0_write(unsigned char data);
static unsigned concatenate(unsigned x, unsigned y);
static void blink_ok(unsigned char count);
static void state_led(void);
static void output_digit(unsigned char digit_index, unsigned char digit);

volatile struct Action {
   unsigned int check_temperature : 1;
   unsigned int print_devices : 1;
   unsigned int mc35i_init_msg_mode : 1;
   unsigned int mc35i_send_next_msg : 1;
   unsigned int mc35i_write_msg_text : 1;
} action;

volatile struct State {
   unsigned int mc35i_on : 1;
   unsigned int mc35i_init_start : 1;
   unsigned int mc35i_wait_text_input : 1;
   unsigned int mc35i_wait_msg_accept : 1;
   unsigned int mc35i_alert_msg_sent : 1;
   unsigned int mc35i_ok_msg_sent : 1;
   unsigned int checking_temperature : 1;
   unsigned int global_warning : 1;
   unsigned int global_alert : 1;
   unsigned int global_ok : 1;
   unsigned int warning_beep : 1;
   unsigned int show_all_sensors : 1;
} state;

unsigned char owDevicesIDs[MAXDEVICES][8];	// ID
FILE usart_str = FDEV_SETUP_STREAM(USART0_write, NULL, _FDEV_SETUP_WRITE); // ��� ������� printf

unsigned char rom[MAXDEVICES][8] = {
	{ 0x28, 0xFF, 0xC5, 0x1B, 0xA3, 0x15, 0x03, 0xF7 },
	{ 0x28, 0x61, 0x4A, 0xAB, 0x06, 0x00, 0x00, 0x52 },
	{ 0x28, 0xFF, 0x53, 0xC6, 0x21, 0x16, 0x03, 0x38 },
	{ 0x28, 0xFF, 0x55, 0xAF, 0x21, 0x16, 0x04, 0x24 },
	{ 0x28, 0xFF, 0xEB, 0xBE, 0x21, 0x16, 0x03, 0xFD },
	{ 0x28, 0xFF, 0x98, 0xBF, 0x21, 0x16, 0x04, 0xD2 },
	{ 0x28, 0xFF, 0xFC, 0x99, 0x21, 0x16, 0x04, 0xA5 },
	{ 0x28, 0xFF, 0xAD, 0xC7, 0x21, 0x16, 0x03, 0xB3 },
	{ 0x28, 0xFF, 0x0E, 0x9F, 0x21, 0x16, 0x04, 0x89 },
	{ 0x28, 0xFF, 0x9C, 0xAF, 0x21, 0x16, 0x04, 0xF5 },
	{ 0x28, 0xFF, 0x3C, 0xB7, 0x21, 0x16, 0x04, 0xE2 }
};

volatile int8_t average_power_supply_temps[MAXDEVICES];
volatile int8_t power_supply_temp_measurements[MEASUREMENT_COUNT][MAXDEVICES];

volatile uint16_t temp_cnt;
volatile uint32_t msg_cnt;
volatile uint16_t warning_beep_cnt;
volatile unsigned char current_measurement;
volatile signed char showing_sensor = -1;
volatile unsigned char current_phone_number;
volatile unsigned char current_digit;

char data_in[25];
volatile unsigned char data_in_count = 0;

static const char mc35i_command_state[] = "AT\r";
static const char mc35i_command_msg_mode[] = "AT+CMGF=1\r";
static const char *phone_number_commands[] = {"AT+CMGS=+79128280679\r"};
//static const char *phone_number_commands[] = {"AT+CMGS=+79195261513\r"};
static const char ctrl_z_char = 26;

// INTERRUPTS
ISR(USART_RX_vect) {
	// Get data from the USART in register
    data_in[data_in_count] = UDR0;
	if (data_in_count > 0 && data_in[data_in_count - 1] == 'O' && data_in[data_in_count] == 'K') {
		if (state.mc35i_init_start) {
			state.mc35i_on = 1;
			state.mc35i_init_start = 0;
			action.mc35i_init_msg_mode = 1;
		} else if (state.mc35i_wait_msg_accept) {
			action.mc35i_send_next_msg = 1;
			state.mc35i_wait_msg_accept = 0;
		}
		// Reset to 0, ready to go again
	} else if ('>' == data_in[data_in_count] && state.mc35i_wait_text_input == 1) {
		action.mc35i_write_msg_text = 1;
		state.mc35i_wait_text_input = 0;
	}
	
	if (data_in[data_in_count] == '\n' || data_in[data_in_count] == '\r') {
        data_in_count = 0;
    } else {
        data_in_count++;
    }

 }// notify main of receipt of data.

ISR(TIMER0_OVF_vect) {

	if (++temp_cnt > 1600) {
		if (!state.checking_temperature) {
			action.check_temperature = 1;
		}
		show_next_sensor();
		temp_cnt = 0;
	}

	if (++msg_cnt > 341600) {
		msg_cnt = 0;
		state.mc35i_wait_text_input = 0;
		state.mc35i_wait_msg_accept = 0;
		state.mc35i_alert_msg_sent = 0;
		state.mc35i_ok_msg_sent = 0;
	}
	
	LED_DISPLAY_SWITCH_OFF();
	if (showing_sensor < 0) {
		return;
	}
	uint16_t data = concatenate((showing_sensor + 1), average_power_supply_temps[showing_sensor]);
	switch(current_digit)
    {
        case 3: output_digit(current_digit, data % 10); break; 
        case 2:	output_digit(current_digit, data % 100 / 10); break;
        case 1: output_digit(current_digit, data % 1000 / 100); break;
        case 0: output_digit(current_digit, data % 10000 / 1000); break;
    }
    if (++current_digit > 3) current_digit = 0;

}

ISR(TIMER2_OVF_vect) {
	if (state.global_alert || (state.global_warning && state.warning_beep)) {
		ibi(BUZZER_PORT, BUZZER_PIN);
	} else {
		cbi(BUZZER_PORT, BUZZER_PIN);
	}
	if (state.global_warning && ++warning_beep_cnt > 2000) {
		warning_beep_cnt = 0;
		if (state.warning_beep) {
			state.warning_beep = 0;
		} else {
			state.warning_beep = 1;
		}
	}
}

// END INTERRUPTS

int main(void) {
	cli();

	io_init();
	//OSCCAL = 0xa9;
	stdout = &usart_str; // ���������, ���� ����� �������� printf 
	state.global_ok = 1;
	USART_init(); // �������� uart

	sei();
	init_mc35i();
	_delay_ms(1500);
	
	if (!state.mc35i_on) {
		blink_ok(5);
	}
	//state.show_all_sensors = 1;
	state_led();
	timer_init();
	while(1) {
		action.check_temperature = 1;
		_delay_ms(4000);
		if (action.mc35i_init_msg_mode) {
			mc35i_init_msg_mode();
		}
		if (action.mc35i_send_next_msg) {
			mc35i_send_next_msg();
		}
		if (action.mc35i_write_msg_text) {
			mc35i_write_msg_text();
		}
		if (action.check_temperature) {
			check_temperature();
		}
		if (action.print_devices) {
			//print_devices();
		}
	}
}

void blink_ok(unsigned char count) {
	for (unsigned char i = 0; i < count; i++) {
		ibi(OK_PORT, OK_PIN);
		_delay_ms(200);	
	}
	cbi(OK_PORT, OK_PIN);
}

void io_init (void) {
	//DDRA = 0b00000000; PORTA = 0b00000000;
	DDRB = 0b00000000; PORTB = 0b00000000;
	DDRC = 0b00000000; PORTC = 0b00000000;
	DDRD = 0b00000000; PORTD = 0b00000000;
	BUZZER_DDR |= 1<<BUZZER_PIN;
	ALERT_DDR |= 1<<ALERT_PIN;
	WARNING_DDR |= 1<<WARNING_PIN;
	OK_DDR |= 1<<OK_PIN; //1 = output, 0 = input
	LED_DISPLAY_INIT();
}

void timer_init(void) {
	TCNT0 = 0x00;
	TCNT2 = 0x00;

	TCCR0B = (1<<CS01)|(1<<CS00); // 64
	TIMSK0 = (1<<TOIE0);

	TCCR2B = (1<<CS21); // 8
	TIMSK2 = (1<<TOIE2);
	
	
	//OCR1A = 31250; // set the CTC compare value + 256 prescaler (1 second)
	//TCCR1B = (1 << WGM12); // configure timer1 for CTC mode
	//TCCR1B |= (1 << CS12); // start the timer at 8MHz/256
	//TIMSK |= (1 << OCIE1A); // enable the CTC interrupt
}

void USART_init() {
	// Set baud rate
	UBRR0L = USART_BAUDRATE_9600;
	UBRR0H = (USART_BAUDRATE_9600 >> 8);
	UCSR0A = 0;

	// Enable receiver and transmitter
	//UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
	UCSR0B = (1<<TXEN0)|(1<<RXEN0)|(1<<RXCIE0);
	// Set frame format
	//UCSR0C = (1<<USBS0) | (1<<UCSZ00);
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1<<UCSZ01)|(1<<UCSZ00);
}

void init_mc35i() {
	state.mc35i_init_start = 1;
	printf(mc35i_command_state);
}

void mc35i_init_msg_mode() {
	action.mc35i_init_msg_mode = 0;
	printf(mc35i_command_msg_mode);
}

void mc35i_send_next_msg() {
	if (current_phone_number >= ARRLEN(phone_number_commands)) {
		current_phone_number = 0;
		state.mc35i_wait_msg_accept = 0;
	} else {
		cli();
		printf(phone_number_commands[current_phone_number]);
		sei();
		state.mc35i_wait_text_input = 1;
		current_phone_number++;
	}
	action.mc35i_send_next_msg = 0;
}

void mc35i_write_msg_text() {
	cli();
	if (state.mc35i_ok_msg_sent) {
		printf("OK!%c", ctrl_z_char);
	} else if (state.mc35i_alert_msg_sent) {
		signed char sensor_number = search_next_overheat_sensor(0, MAXDEVICES);
		
		if (sensor_number >= 0) {
			char alert_buf[100];
			sprintf(alert_buf, "ALERT! ");
			for (unsigned char i = sensor_number; i < MAXDEVICES; i ++) {
				if (is_overheat(i)) {
					sprintf(alert_buf, "%d: %d ", i + 1, average_power_supply_temps[i]);
				}
			}
			printf("%s%c", alert_buf, ctrl_z_char);
		}
	}
	sei();
	state.mc35i_wait_msg_accept = 1;
	action.mc35i_write_msg_text = 0;
}

void check_temperature() {
	action.check_temperature = 0;
	state.checking_temperature = 1;
	
	for (unsigned char i = 0; i < MAXDEVICES; i ++) {
		cli();
		DS18x20_StartMeasure(rom[i]); // ��������� ���������
		sei();
		_delay_ms(800); // ���� ������� 750 ��, ���� �������������� �����������
		unsigned char data[2]; // ���������� ��� �������� �������� � �������� ����� ������
		cli();
		DS18x20_ReadData(rom[i], data); // ��������� ������s
		sei();
		float t = DS18x20_ConvertToThemperatureFl(data); // ��������������� ����������� � ���������������� ���
		if (t > 99) {
			power_supply_temp_measurements[current_measurement][i] = 99;
		} else if (t < 0) {
			power_supply_temp_measurements[current_measurement][i] = 0;
		} else {
			power_supply_temp_measurements[current_measurement][i] = (uint8_t)t;
		}
	}

	if (++current_measurement == MEASUREMENT_COUNT) {
		unsigned char current_overheat = state.global_alert || state.global_warning;
		state.global_ok = 1;
		state.global_alert = 0;
		state.global_warning = 0;
		current_measurement = 0;
		
		calculate_average_temperatures();
		
		for (unsigned char i = 0; i < MAXDEVICES; i ++) {
			const uint8_t t = average_power_supply_temps[i];
			if ((i == OFFICE_TEMP_SENSOR_INDEX && t >= OFFICE_TEMP_ALERT) ||
				(i != OFFICE_TEMP_SENSOR_INDEX && t >= POWER_SUPPLY_TEMP_ALERT)) {
				state.global_alert = 1;
				state.global_ok = 0;
				state.global_warning = 0;
			} else if (((i == OFFICE_TEMP_SENSOR_INDEX && t >= OFFICE_TEMP_WARNING) ||
				(i != OFFICE_TEMP_SENSOR_INDEX && t >= POWER_SUPPLY_TEMP_WARNING)) &&
				!state.global_alert) {
				state.global_warning = 1;
				state.global_ok = 0;
				state.global_alert = 0;
			}
		}

		state_led();
		
		if (state.global_alert && state.mc35i_on && !state.mc35i_alert_msg_sent && !state.mc35i_ok_msg_sent) {
			action.mc35i_send_next_msg = 1;
			state.mc35i_alert_msg_sent = 1;
		} else if (state.global_ok && current_overheat) {
			action.mc35i_send_next_msg = 1;
			state.mc35i_ok_msg_sent = 1;
		}
	}
	state.checking_temperature = 0;
}

void calculate_average_temperatures() {
	for (unsigned char i = 0; i < MAXDEVICES; i ++) {
		uint16_t temp_sum = 0;
		for (unsigned char j = 0; j < MEASUREMENT_COUNT; j ++) {
			temp_sum += power_supply_temp_measurements[j][i];
		}
		average_power_supply_temps[i] = (uint8_t) (temp_sum/MEASUREMENT_COUNT);
	}
}

void show_next_sensor() {
	cli();
	if (state.show_all_sensors) {
		showing_sensor = ((showing_sensor == MAXDEVICES - 1) ? 0 : showing_sensor + 1);
	} else {
		signed char found = -1;

		if (showing_sensor < MAXDEVICES - 1) {
			found = search_next_overheat_sensor(showing_sensor + 1, MAXDEVICES);
		}
		if (found == -1) {
			found = search_next_overheat_sensor(0, showing_sensor + 1);
		}
		showing_sensor = found;	
	} 
	sei();
}

signed char search_next_overheat_sensor(signed char start, signed char end) {
	signed char found = -1;
	for (signed char i = start; i < end; i ++) {
		if (is_overheat(i)) {
			found = i;
			break;
		}
	}
	return found;
}

unsigned char is_overheat(signed char sensor) {
	return (sensor != OFFICE_TEMP_SENSOR_INDEX && (average_power_supply_temps[sensor] >= POWER_SUPPLY_TEMP_WARNING || average_power_supply_temps[sensor] >= POWER_SUPPLY_TEMP_ALERT))
			|| (sensor == OFFICE_TEMP_SENSOR_INDEX && (average_power_supply_temps[sensor] >= OFFICE_TEMP_WARNING || average_power_supply_temps[sensor] >= OFFICE_TEMP_ALERT));
}

void print_devices() {
	cli();

	for (unsigned char i=0; i<search_ow_devices(); i++) { // ������ �������� ���������� � ����������� ������
		// ������ ���������� ����� �� ��� ��������� ����, ������� ���������� � ������ ����� �������
		print_address(owDevicesIDs[i]); // �������� �����
		DS18x20_StartMeasure(rom[i]); // ��������� ���������
		_delay_ms(800); // ���� ������� 750 ��, ���� �������������� �����������
		unsigned char data[2]; // ���������� ��� �������� �������� � �������� ����� ������
		DS18x20_ReadData(rom[i], data); // ��������� ������s
		printf(" %d \n", (uint8_t)DS18x20_ConvertToThemperatureFl(data));
	}
	printf("\n");
	sei();
}

void USART0_write(unsigned char data)
{
	while ( !( UCSR0A & (1<<UDRE0)) ) ;
	UDR0 = data;
}

void print_address(unsigned char* address) {
	printf("%.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X", address[0],address[1],address[2],address[3],address[4],address[5],address[6],address[7]);
}

unsigned char search_ow_devices(void) // ����� ���� ��������� �� ����
{ 
   	unsigned char id[OW_ROMCODE_SIZE];
   	unsigned char diff;
	unsigned char sensors_count = 0;

	for( diff = OW_SEARCH_FIRST; diff != OW_LAST_DEVICE && sensors_count < MAXDEVICES ; sensors_count++)
    {
		OW_FindROM( &diff, &id[0] );

      	if( diff == OW_PRESENCE_ERR || diff == OW_DATA_ERR ) break;

      	for (unsigned char i=0;i<OW_ROMCODE_SIZE;i++)
         	owDevicesIDs[sensors_count][i] = id[i];
    }
	return sensors_count;

}

unsigned concatenate(unsigned x, unsigned y) {
    int pow = 10;
    while(y >= pow)
        pow *= 10;
    return x * pow + y;
}

void state_led(void) {
	STATE_LED_SWITCH_OFF();
	if (state.global_alert) {
		sbi(ALERT_PORT, ALERT_PIN);
	} else if (state.global_warning) {
		sbi(WARNING_PORT, WARNING_PIN);
	} else {
		sbi(OK_PORT, OK_PIN);
	}
}

void output_digit(unsigned char digit_index, unsigned char digit) {
	DP_PORT |= 1<<DP_PIN;
	switch(digit_index) {
		case 0:
		COM1_PORT |= 1<<COM1_PIN;
		break;
		case 1:
		COM2_PORT |= 1<<COM2_PIN;
		DP_PORT &= ~(1<<DP_PIN);
		break;
		case 2:
		COM3_PORT |= 1<<COM3_PIN;
		break;
		case 3:
		COM4_PORT |= 1<<COM4_PIN;
		break;
	}
	switch(digit) {
		case 0:
		ZERO_ON();
		break;
		case 1:
		ONE_ON();
		break;
		case 2:
		TWO_ON();
		break;
		case 3:
		THREE_ON();
		break;
		case 4:
		FOUR_ON();
		break;
		case 5:
		FIVE_ON();
		break;
		case 6:
		SIX_ON();
		break;
		case 7:
		SEVEN_ON();
		break;
		case 8:
		EIGHT_ON();
		break;
		case 9:
		NINE_ON();
		break;
	}
}
